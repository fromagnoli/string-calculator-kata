<?php
namespace Fabiana\StringCalculatorKata;

class StringCalculator
{

    public function add(string $numbers): int
    {
        list($delimiters, $numbers) = $this->separateDelimitersFromNumbers($numbers);
        $finalDelimiter = $delimiters[0];

        foreach ($delimiters as $eachDelimiter) {
            $numbers = str_replace($eachDelimiter, $finalDelimiter, $numbers);
        }

        $intNumbers = explode($finalDelimiter, $numbers);
        $intNumbers = $this->removeBigNumbers($intNumbers);

        $negativeNumbers = $this->findNegativeNumbers($intNumbers);
        if (!empty($negativeNumbers)) {
            throw new \Exception('negatives not allowed, "'.implode(', ', $negativeNumbers).'" given');
        }

        return array_reduce($intNumbers, function ($counter, $item) {
            $counter += $item;
            return $counter;
        }, 0);



    }


    private function separateDelimitersFromNumbers(string $numbers): array
    {
        $defaultDelimiters = [",", "\n"];
        $thereIsACustomDelimiter = (substr($numbers, 0, 2) == '//');

        $delimiters = $defaultDelimiters;
        if ($thereIsACustomDelimiter) {
            $numbers = substr($numbers, 2);
            $thereIsALongOrMultipleDelimiter = preg_match('/^\[.+\]+/', $numbers, $matches, PREG_OFFSET_CAPTURE);

            if ($thereIsALongOrMultipleDelimiter) {
                $delimiterMatch = $matches[0][0];
                $delimiter = trim($delimiterMatch, '[]');
                $customDelimiters = explode('][', $delimiter);
                $delimiters = array_merge($defaultDelimiters, $customDelimiters);
                $numbers = substr($numbers, strlen($delimiterMatch));
            } else {
                array_push($delimiters, substr($numbers, 0, 1));
                $numbers = substr($numbers, 1);
            }

        }
        return array($delimiters, $numbers);
    }


    private function removeBigNumbers(array $intNumbers): array
    {
        return array_filter($intNumbers, function ($eachItem) {
            return ($eachItem <= 1000);
        });
    }

    private function findNegativeNumbers(array $intNumbers): array
    {
        return array_filter($intNumbers, function ($eachItem) {
            return ($eachItem < 0);
        });
    }

}

